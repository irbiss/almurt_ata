from django.db import models


class Pizza(models.Model):
    DOUGH_CHOICES = [
        ('THICK', 'Толстое'),
        ('THIN', 'Тонкое'),
    ]
    name = models.CharField(max_length=200, verbose_name='Name')
    restaurant = models.ForeignKey(
        to='restaurants.Restaurant',
        on_delete=models.CASCADE,
        related_name='pizzas',
        verbose_name='Restaurant'
    )
    dough = models.CharField(
        max_length=200,
        choices=DOUGH_CHOICES,
        default='THICK',
        verbose_name='Dough'
    )
    cheese_type = models.CharField(max_length=200, verbose_name='Type of cheese')
    secret_ingredient = models.TextField(max_length=800, verbose_name='Secret ingredient')
