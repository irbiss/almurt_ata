from django.db import models


class Restaurant(models.Model):
    name = models.CharField(max_length=200, verbose_name='Name')
    address = models.CharField(max_length=1200, verbose_name='Address')

    def __str__(self):
        return f'{self.name} restaurant'
