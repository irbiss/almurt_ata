from rest_framework import viewsets

from apps.api.serializers import RestaurantSerializer, PizzaSerializer
from apps.restaurants.models import Restaurant
from apps.pizzas.models import Pizza


class RestaurantViewSet(viewsets.ModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class PizzasViewSet(viewsets.ModelViewSet):
    queryset = Pizza.objects.all()
    serializer_class = PizzaSerializer
