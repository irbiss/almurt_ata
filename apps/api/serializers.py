from rest_framework import serializers

from apps.pizzas.models import Pizza
from apps.restaurants.models import Restaurant


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['name', 'address']


class PizzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pizza
        fields = ['name', 'restaurant', 'dough', 'cheese_type', 'secret_ingredient']
