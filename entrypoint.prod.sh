#!/bin/bash

python manage.py migrate
if ["$DEBUG" = ""]
then
  python manage.py collectstatic --noinput
fi
gunicorn core.wsgi:application --bind 0.0.0.0:8000 --workers 3

exec "$@"