## How to use API
### 1.  Restaurant model

Get list
``` 
http://185.125.88.61/api/restaurants/ GET 
```
    
Create new
    
```
http://185.125.88.61/api/restaurants/ POST 
```

Update

```
http://185.125.88.61/api/restaurants/<id> PUT / PATCH
```
Delete

```
http://185.125.88.61/api/restaurants/<id> DELETE 
```
### 2. Pizza model

Get list

```
http://185.125.88.61/api/pizzas/ GET
```
Create new
    
```
http://185.125.88.61/api/pizzas/ POST 
```

Update

```
http://185.125.88.61/api/pizzas/<id> PUT / PATCH
```
Delete

```
http://185.125.88.61/api/pizzas/<id> DELETE 
```
